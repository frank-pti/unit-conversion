﻿/*
 * A library for converting between different physical units
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace FrankPti.UnitConversion
{
    /// <summary>
    /// 
    /// </summary>
    public class Mass : Unit<MassUnit>
    {
        private const double GramToMilliFactor = 1000;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrankPti.UnitConversion.Mass"/> class.
        /// </summary>
        /// <param name="val">Value.</param>
        /// <param name="unit">Unit.</param>
        public Mass(double val, MassUnit unit) :
            base(val, unit)
        {
        }

        /// <summary>
        /// Gets the value in the target unit
        /// </summary>
        /// <param name="targetUnit">Unit.</param>
        /// <returns>Value.</returns>
        public override double GetValue(MassUnit targetUnit)
        {
            switch (targetUnit) {
                case MassUnit.Gram:
                    return GetValueInGram();
                case MassUnit.MilliGram:
                    return GetValueInMilliGram();
            }
            return base.GetValue(targetUnit);
        }

        private double GetValueInGram()
        {
            switch (unit) {
                case MassUnit.Gram:
                    return val;
                case MassUnit.MilliGram:
                    return FromMilliGramToGram(val);
            }
            return NotifyInvalidUnit(unit);
        }

        private double GetValueInMilliGram()
        {
            switch (unit) {
                case MassUnit.Gram:
                    return FromGramToMilliGram(val);
                case MassUnit.MilliGram:
                    return val;
            }
            return NotifyInvalidUnit(unit);
        }

        /// <summary>
        /// Gets the value in gram
        /// </summary>
        public double Gram
        {
            get
            {
                return GetValue(MassUnit.Gram);
            }
        }

        /// <summary>
        /// Gets the value in milli gram
        /// </summary>
        public double MilliGram
        {
            get
            {
                return GetValue(MassUnit.MilliGram);
            }
        }

        /// <summary>
        /// Convert from gram to milli gram
        /// </summary>
        /// <param name="val">The value in gram</param>
        /// <returns>The value in milli gram</returns>
        public static double FromGramToMilliGram(double val)
        {
            return val * GramToMilliFactor;
        }

        /// <summary>
        /// Convert from milli gram to gram
        /// </summary>
        /// <param name="val">The value in milli gram</param>
        /// <returns>The value in gram</returns>
        private static double FromMilliGramToGram(double val)
        {
            return val / GramToMilliFactor;
        }
    }
}
