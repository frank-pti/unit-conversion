﻿/*
 * A library for converting between different physical units
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.ComponentModel;

namespace FrankPti.UnitConversion
{
    /// <summary>
    /// Enumeration of mass units
    /// </summary>
    public enum MassUnit
    {
        /// <summary>
        /// Gram is one thousandth of the SI unit kilo gram
        /// </summary>
        [Description("g")]
        Gram,

        /// <summary>
        /// Milli gram is one thousandth of a gram
        /// </summary>
        [Description("mg")]
        MilliGram
    }
}
