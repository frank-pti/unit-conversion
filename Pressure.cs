/*
 * A library for converting between different physical units
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace FrankPti.UnitConversion
{
    /// <summary>
    /// Class for converting pressure units.
    /// </summary>
    public class Pressure : Unit<PressureUnit>
    {
        private const double BarToPsiFactor = 14.5038;
        private const double BarToKPaFactor = 100;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrankPti.UnitConversion.Pressure"/> class.
        /// </summary>
        /// <param name="val">Value.</param>
        /// <param name="unit">Unit.</param>
        public Pressure(double val, PressureUnit unit) :
            base(val, unit)
        {
        }

        /// <summary>
        /// Gets the value in the target unit.
        /// </summary>
        /// <returns>The value in the target unit.</returns>
        /// <param name="targetUnit">Target unit.</param>
        public override double GetValue(PressureUnit targetUnit)
        {
            switch (targetUnit) {
                case PressureUnit.bar:
                    return GetValueInBar();
                case PressureUnit.kPa:
                    return GetValueInKPa();
                case PressureUnit.psi:
                    return GetValueInPsi();
            }
            return base.GetValue(targetUnit);
        }

        private double GetValueInBar()
        {
            switch (unit) {
                case PressureUnit.bar:
                    return val;
                case PressureUnit.kPa:
                    return FromKPaToBar(val);
                case PressureUnit.psi:
                    return FromPsiToBar(val);
            }
            return NotifyInvalidUnit(unit);
        }

        private double GetValueInKPa()
        {
            switch (unit) {
                case PressureUnit.bar:
                    return FromBarToKPa(val);
                case PressureUnit.kPa:
                    return val;
                case PressureUnit.psi:
                    return FromPsiToKPa(val);
            }
            return NotifyInvalidUnit(unit);
        }

        private double GetValueInPsi()
        {
            switch (unit) {
                case PressureUnit.bar:
                    return FromBarToPsi(val);
                case PressureUnit.kPa:
                    return FromKPaToPsi(val);
                case PressureUnit.psi:
                    return val;
            }
            return NotifyInvalidUnit(unit);
        }

        /// <summary>
        /// Get the value in kPa.
        /// </summary>
        /// <value>The value in kPa.</value>
        public double KPa
        {
            get
            {
                return GetValue(PressureUnit.kPa);
            }
        }

        /// <summary>
        /// Gets the value in bar.
        /// </summary>
        /// <value>The value in bar.</value>
        public double Bar
        {
            get
            {
                return GetValue(PressureUnit.bar);
            }
        }

        /// <summary>
        /// Gets the value in psi.
        /// </summary>
        /// <value>The value in psi.</value>
        public double Psi
        {
            get
            {
                return GetValue(PressureUnit.psi);
            }
        }

        /// <summary>
        /// Convert from bar to kPa.
        /// </summary>
        /// <returns>Value in kPa.</returns>
        /// <param name="bar">Value in bar.</param>
        public static double FromBarToKPa(double bar)
        {
            return bar * BarToKPaFactor;
        }

        /// <summary>
        /// Convert from kPa to bar.
        /// </summary>
        /// <returns>Value in kPa.</returns>
        /// <param name="kPa">Value in bar.</param>
        public static double FromKPaToBar(double kPa)
        {
            return kPa / BarToKPaFactor;
        }

        /// <summary>
        /// Convert from bar to psi.
        /// </summary>
        /// <returns>Value in psi.</returns>
        /// <param name="bar">Value in bar.</param>
        public static double FromBarToPsi(double bar)
        {
            return bar * BarToPsiFactor;
        }

        /// <summary>
        /// Convert from psi to bar.
        /// </summary>
        /// <returns>Value in bar.</returns>
        /// <param name="psi">Value in psi.</param>
        public static double FromPsiToBar(double psi)
        {
            return psi / BarToPsiFactor;
        }

        /// <summary>
        /// Convert from psi to kPa.
        /// </summary>
        /// <returns>Value in kPa.</returns>
        /// <param name="psi">Value in psi.</param>
        public static double FromPsiToKPa(double psi)
        {
            return FromBarToKPa(FromPsiToBar(psi));
        }

        /// <summary>
        /// Convert from KPa to psi
        /// </summary>
        /// <returns>Value in psi.</returns>
        /// <param name="kPa">Value in kPa.</param>
        public static double FromKPaToPsi(double kPa)
        {
            return FromBarToPsi(FromKPaToBar(kPa));
        }
    }
}

