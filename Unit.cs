﻿/*
 * A library for converting between different physical units
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;

namespace FrankPti.UnitConversion
{
    /// <summary>
    /// Base class for unit classes
    /// </summary>
    /// <typeparam name="T">The unit enum type</typeparam>
    public abstract class Unit<T>
        where T : struct
    {
        /// <summary>Value</summary>
        protected double val;
        /// <summary>Unit</summary>
        protected T unit;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="val">Value</param>
        /// <param name="unit">Unit</param>
        protected Unit(double val, T unit)
        {
            this.val = val;
            this.unit = unit;
        }

        /// <summary>
        /// Gets the value in the target unit.
        /// </summary>
        /// <param name="targetUnit">The unit.</param>
        /// <returns>The value in the target unit</returns>
        public virtual double GetValue(T targetUnit)
        {
            throw new ArgumentException(String.Format("Invalid target unit: {0}", targetUnit));
        }

        /// <summary>
        /// Throw argument exception
        /// </summary>
        /// <param name="unit">invalid unit</param>
        /// <returns>Nothing, an argument exception is thrown in any case.</returns>
        protected double NotifyInvalidUnit(T unit)
        {
            throw new ArgumentException(String.Format("Invalid unit: {0}", unit));
        }
    }
}
