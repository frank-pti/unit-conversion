﻿/*
 * A library for converting between different physical units
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace FrankPti.UnitConversion
{
    /// <summary>
    /// Class for converting length units
    /// </summary>
    public class Length : Unit<LengthUnit>
    {
        private const double MilliMeterFactor = 1000;
        private const double MicroMeterFactor = 1000000;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrankPti.UnitConversion.Length"/> class.
        /// </summary>
        /// <param name="val">Value.</param>
        /// <param name="unit">Unit.</param>
        public Length(double val, LengthUnit unit) :
            base(val, unit)
        {
        }

        /// <summary>
        /// Gets the value in the target unit.
        /// </summary>
        /// <param name="targetUnit">Target unit.</param>
        /// <returns>The value in the target unit.</returns>
        public override double GetValue(LengthUnit targetUnit)
        {
            switch (targetUnit) {
                case LengthUnit.Meter:
                    return GetValueInMeter();
                case LengthUnit.MilliMeter:
                    return GetValueInMilliMeter();
                case LengthUnit.MicroMeter:
                    return GetValueInMicroMeter();
            }
            return base.GetValue(targetUnit);
        }

        private double GetValueInMicroMeter()
        {
            switch (unit) {
                case LengthUnit.MicroMeter:
                    return val;
                case LengthUnit.MilliMeter:
                    return FromMilliToMicro(val);
                case LengthUnit.Meter:
                    return FromOneToMicro(val);
            }
            return NotifyInvalidUnit(unit);
        }

        private double GetValueInMilliMeter()
        {
            switch (unit) {
                case LengthUnit.MicroMeter:
                    return FromMicroToMilli(val);
                case LengthUnit.MilliMeter:
                    return val;
                case LengthUnit.Meter:
                    return FromMicroToOne(val);
            }
            return NotifyInvalidUnit(unit);
        }

        private double GetValueInMeter()
        {
            switch (unit) {
                case LengthUnit.MicroMeter:
                    return FromOneToMicro(val);
                case LengthUnit.MilliMeter:
                    return FromOneToMilli(val);
                case LengthUnit.Meter:
                    return val;
            }
            return NotifyInvalidUnit(unit);
        }

        /// <summary>
        /// Gets the value in meters.
        /// </summary>
        public double Meter
        {
            get
            {
                return GetValue(LengthUnit.Meter);
            }
        }

        /// <summary>
        /// Gets the value in milli meters.
        /// </summary>
        public double MilliMeter
        {
            get
            {
                return GetValue(LengthUnit.MilliMeter);
            }
        }

        /// <summary>
        /// Getst the value in micro meters.
        /// </summary>
        public double MicroMeter
        {
            get
            {
                return GetValue(LengthUnit.MicroMeter);
            }
        }

        /// <summary>
        /// Convert from milli meters to micro meters.
        /// </summary>
        /// <param name="val">The value in milli meters</param>
        /// <returns>The value in micro meters</returns>
        public static double FromMilliToMicro(double val)
        {
            return FromOneToMicro(FromMilliToOne(val));
        }

        /// <summary>
        /// Converts from micro meters to milli meters
        /// </summary>
        /// <param name="val">The value in micro meters</param>
        /// <returns>The value in milli meters</returns>
        public static double FromMicroToMilli(double val)
        {
            return FromOneToMilli(FromMicroToOne(val));
        }

        /// <summary>
        /// Converts from meters to milli meters
        /// </summary>
        /// <param name="val">The value in meters</param>
        /// <returns>The value in milli meters</returns>
        public static double FromOneToMilli(double val)
        {
            return val * MilliMeterFactor;
        }

        /// <summary>
        /// Converts from milli meiters to meters
        /// </summary>
        /// <param name="val">The value in milli meters</param>
        /// <returns>The value in meters</returns>
        public static double FromMilliToOne(double val)
        {
            return val / MilliMeterFactor;
        }

        /// <summary>
        /// Converts from meters to micro meters
        /// </summary>
        /// <param name="val">The value in meters</param>
        /// <returns>The value in micro meters</returns>
        public static double FromOneToMicro(double val)
        {
            return val * MicroMeterFactor;
        }

        /// <summary>
        /// Converts from micro meters to meters
        /// </summary>
        /// <param name="val">The value in micro meters</param>
        /// <returns>The value in meters</returns>
        public static double FromMicroToOne(double val)
        {
            return val / MicroMeterFactor;
        }
    }
}
