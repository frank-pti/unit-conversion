/*
 * A library for converting between different physical units
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel;

namespace FrankPti.UnitConversion
{
    /// <summary>
    /// Enumeration of the pressure units.
    /// </summary>
	public enum PressureUnit
	{
        /// <summary>
        /// Pascal is the SI derived unit of pressure. It is a measure of force per unit area, defined as N/m².
        /// </summary>
        /// <remarks>1 kPa = 1,000 Pa = 1,000 N/m².</remarks>
		[Description("kPa")]
		kPa,

        /// <summary>
        /// The bar is a non-SI unit of pressure, defined by the IUPAC as exactly equal to 100,000 Pa (= 100 kPa)
        /// </summary>
        /// <remarks>1 bar = 100 kPa = 100,000 Pa.</remarks>
		[Description("bar")]
		bar,

        /// <summary>
        /// The pound per square inch or, more accurately, pound-force per square inch (psi) is a unit of pressure
        /// based on avoirdupois units.
        /// </summary>
		[Description("psi")]
		psi
	}
}

